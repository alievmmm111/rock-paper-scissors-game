from colorama import Fore, Style, init
import hashlib
import sys
import random
import hmac

init()

class Game:
    def __init__(self, moves):
        self.validate_moves(moves)
        self.moves = moves
        self.key = self.generate_key()
        self.computer_move = None
        self.user_move = None

    @staticmethod
    def validate_moves(moves):
        if len(moves) != len(set(moves)):
            print("Error: Please provide non-repeating moves.")
            sys.exit(1)

    @staticmethod
    def generate_key():
        return bytes(random.getrandbits(8) for _ in range(32))

    def compute_hmac(self, move):
        h = hmac.new(self.key, msg=move.encode('utf-8'), digestmod=hashlib.sha256)
        return h.hexdigest()

    def get_computer_move(self):
        return random.choice(self.moves)

    def play(self):
        print(f'{Fore.YELLOW}HMAC:{Style.RESET_ALL} {self.compute_hmac(self.get_computer_move())}')
        self.display_menu()
        while True:
            user_input = input('Enter your move: ')
            if user_input.isdigit():
                user_choice = int(user_input)
                if 0 <= user_choice <= len(self.moves):
                    if user_choice == 0:
                        print('Exiting the game.')
                        break
                    else:
                        self.user_move = self.moves[user_choice - 1]
                        self.computer_move = self.get_computer_move()
                        self.display_results()
                else:
                    print('Invalid input. Please enter a valid move number.')
            elif user_input == '?':
                self.display_help_table()
            else:
                print('Invalid input. Please enter a valid move number or "?" for help.')

    def display_menu(self):
        print('Available moves:')
        for i, move in enumerate(self.moves):
            print(f'{i + 1} - {move}')
        print('0 - exit')
        print('? - help')

    def display_results(self):
        print(f'{Fore.GREEN}Your move:{Style.RESET_ALL} {self.user_move}')
        print(f'{Fore.RED}Computer move:{Style.RESET_ALL} {self.computer_move}')
        result = self.calculate_winner()
        print(result)
        print(f'{Fore.YELLOW}HMAC key:{Style.RESET_ALL} {self.key.hex()}')

    def calculate_winner(self):
        n = len(self.moves)
        half_n = n // 2
        user_index = self.moves.index(self.user_move)
        computer_index = self.moves.index(self.computer_move)
        if user_index == computer_index:
            return f'{Fore.BLUE}It\'s a draw!{Style.RESET_ALL}'
        elif (user_index - computer_index) % n <= half_n:
            return f'{Fore.GREEN}You win gratz!!{Style.RESET_ALL}'
        else:
            return f'{Fore.RED}Computer wins, sorry pal :({Style.RESET_ALL}'

    def display_help_table(self):
        n = len(self.moves)
        table = [['' for _ in range(n + 1)] for _ in range(n + 1)]
        for i in range(n):
            table[0][0] = f'{Fore.GREEN}PC{Style.RESET_ALL}\\{Fore.RED}User {Style.RESET_ALL}'
            table[i + 1][0] = self.moves[i]
            table[0][i + 1] = self.moves[i]

        for i in range(n):
            for j in range(n):
                result = self.calculate_help_result(self.moves[i], self.moves[j])
                table[i + 1][j + 1] = result


        self.print_table(table)

    def calculate_help_result(self, move1, move2):
        n = len(self.moves)
        move1_index = self.moves.index(move1)
        move2_index = self.moves.index(move2)
        if move1_index == move2_index:
            return 'Draw'
        elif (move1_index - move2_index) % n <= n // 2:
            return 'Win'
        else:
            return 'Lose'

    @staticmethod
    def print_table(table):
        for row in table:
            row_str = '|'.join(f'{cell:^8}' for cell in row)

            row_str = row_str.replace('PC', f'{Fore.RED}PC{Style.RESET_ALL}')
            row_str = row_str.replace('User', f'{Fore.GREEN}User{Style.RESET_ALL}')
            print(f'|{row_str}|')

if __name__ == "__main__":
    if len(sys.argv) < 3 or len(sys.argv) % 2 == 1:
        print("Usage: python game.py <move1> <move2> ... <moveN>")
        print("Please provide an odd number of non-repeating moves.")
    else:
        moves = sys.argv[1:]
        game = Game(moves)
        game.play()