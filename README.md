## Rock-Paper-Scissors Extended

This Python command-line game allows you to play an extended version of Rock-Paper-Scissors against the computer. The game also includes a help table to display the results of all possible move combinations.
Prerequisites

Before running the game, make sure you have Python 3.x installed on your system. Additionally, you will need the colorama library for better console output, which can be installed using pip:

bash

pip install colorama

## Usage

To play the game, execute the following command in your terminal:

bash

python game.py <move1> <move2> ... <moveN>

Replace <move1>, <move2>, etc., with the available moves you want to play with. For example:

bash

python game.py rock paper scissors lizard spock

The number of moves must be an odd number, and each move should be unique.
## Gameplay

    The computer generates a random move.
    The HMAC (Hash-based Message Authentication Code) of the computer's move is displayed to ensure fairness.
    You can choose your move by entering the corresponding number or use '?' to display a help table.
    After making your choice, the game displays your move, the computer's move, and the result.
    The game continues until you choose to exit (enter 0).

## Extended Rules

The extended version of Rock-Paper-Scissors uses the following rules:

    Each move can win against some moves and lose against others.
    The help table displays the results of all possible move combinations.

## Example

bash

python game.py rock paper scissors lizard spock

## Acknowledgments

This game was created by Aliyev Maxmudjon.